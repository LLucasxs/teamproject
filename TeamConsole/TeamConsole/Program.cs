﻿using System;

namespace TeamConsole
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int sum = 0;

            Console.Write("Please enter a number: ");
            int userInput = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i <= userInput; i++)
            {
                Console.Write(i + " ");
                sum = sum + i;
            }
            Console.WriteLine();
            Console.WriteLine("The sum is: " + sum);
        }
    }
}
